<?php

/**
 * 361GRAD Element Gmapmultipin
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = "DSE-Elemente";
$GLOBALS['TL_LANG']['CTE']['dse_gmapmultipin'] = ["Google Map (Multilokation)", "Google Map (Multilokation)"];

$GLOBALS['TL_LANG']['tl_content']['dse_zoomfactor'] =
    ['Zoomfaktor', 'Bitte geben Sie den Zoomfaktor ein.'];

$GLOBALS['TL_LANG']['tl_content']['location1_legend']   = 'Erster Ort';
$GLOBALS['TL_LANG']['tl_content']['dse_location1_latitude'] =
    ['Breite', 'Bitte geben Sie den Breitengrad ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_longitude'] =
    ['Länge', 'Bitte geben Sie den Längengrad ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_company'] =
    ['Unternehmen', 'Bitte geben Sie den Firmennamen ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_street'] =
    ['Straße/Zahl', 'Bitte geben Sie die Straße/Zahl info ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_zip'] =
    ['ZIP/Stadt', 'Bitte geben Sie die ZIP/Stadt Info ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_partnername'] =
    ['Partnername', 'Bitte geben Sie den Namen des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_phone'] =
    ['Partner Telefon', 'Bitte geben Sie das Telefon des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_mail'] =
    ['Partner E-Mail', 'Bitte geben Sie die E-Mail des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_maplink'] =
    ['Roadmap link', 'Hier können Sie eine Roadmap-Link hinzufügen.'];

$GLOBALS['TL_LANG']['tl_content']['location2_legend']   = 'Zweiter Ort';
$GLOBALS['TL_LANG']['tl_content']['dse_location2_latitude'] =
    ['Breite', 'Bitte geben Sie den Breitengrad ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_longitude'] =
    ['Länge', 'Bitte geben Sie den Längengrad ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_company'] =
    ['Titel', 'Bitte geben Sie die Titelinfo ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_street'] =
    ['Straße/Zahl', 'Bitte geben Sie die Straße/Zahl info ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_zip'] =
    ['ZIP/Stadt', 'Bitte geben Sie die ZIP/Stadt Info ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_partnername'] =
    ['Partnername', 'Bitte geben Sie den Namen des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_phone'] =
    ['Partner Telefon', 'Bitte geben Sie das Telefon des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_mail'] =
    ['Partner E-Mail', 'Bitte geben Sie die E-Mail des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_maplink'] =
    ['Roadmap link', 'Hier können Sie eine Roadmap-Link hinzufügen.'];

$GLOBALS['TL_LANG']['tl_content']['location3_legend']   = 'Dritter Ort';
$GLOBALS['TL_LANG']['tl_content']['dse_location3_latitude'] =
    ['Breite', 'Bitte geben Sie den Breitengrad ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_longitude'] =
    ['Länge', 'Bitte geben Sie den Längengrad ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_company'] =
    ['Titel', 'Bitte geben Sie die Titelinfo ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_street'] =
    ['Straße/Zahl', 'Bitte geben Sie die Straße/Zahl info ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_zip'] =
    ['ZIP/Stadt', 'Bitte geben Sie die ZIP/Stadt Info ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_partnername'] =
    ['Partnername', 'Bitte geben Sie den Namen des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_phone'] =
    ['Partner Telefon', 'Bitte geben Sie das Telefon des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_mail'] =
    ['Partner E-Mail', 'Bitte geben Sie die E-Mail des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_maplink'] =
    ['Roadmap link', 'Hier können Sie eine Roadmap-Link hinzufügen.'];

$GLOBALS['TL_LANG']['tl_content']['location4_legend']   = 'Vierte Ort';
$GLOBALS['TL_LANG']['tl_content']['dse_location4_latitude'] =
    ['Breite', 'Bitte geben Sie den Breitengrad ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location4_longitude'] =
    ['Länge', 'Bitte geben Sie den Längengrad ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location4_company'] =
    ['Titel', 'Bitte geben Sie die Titelinfo ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location4_street'] =
    ['Straße/Zahl', 'Bitte geben Sie die Straße/Zahl info ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location4_zip'] =
    ['ZIP/Stadt', 'Bitte geben Sie die ZIP/Stadt Info ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_partnername'] =
    ['Partnername', 'Bitte geben Sie den Namen des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_phone'] =
    ['Partner Telefon', 'Bitte geben Sie das Telefon des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_mail'] =
    ['Partner E-Mail', 'Bitte geben Sie die E-Mail des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location4_maplink'] =
    ['Roadmap link', 'Hier können Sie eine Roadmap-Link hinzufügen.'];

$GLOBALS['TL_LANG']['tl_content']['location5_legend']   = 'Fünfte Ort';
$GLOBALS['TL_LANG']['tl_content']['dse_location5_latitude'] =
    ['Breite', 'Bitte geben Sie den Breitengrad ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_longitude'] =
    ['Länge', 'Bitte geben Sie den Längengrad ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_company'] =
    ['Titel', 'Bitte geben Sie die Titelinfo ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_street'] =
    ['Straße/Zahl', 'Bitte geben Sie die Straße/Zahl info ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_zip'] =
    ['ZIP/Stadt', 'Bitte geben Sie die ZIP/Stadt Info ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_partnername'] =
    ['Partnername', 'Bitte geben Sie den Namen des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_phone'] =
    ['Partner Telefon', 'Bitte geben Sie das Telefon des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_mail'] =
    ['Partner E-Mail', 'Bitte geben Sie die E-Mail des Partners ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_maplink'] =
    ['Roadmap link', 'Hier können Sie eine Roadmap-Link hinzufügen.'];