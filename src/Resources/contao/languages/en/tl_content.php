<?php

/**
 * 361GRAD Element Gmapmultipin
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = "DSE-Elements";
$GLOBALS['TL_LANG']['CTE']['dse_gmapmultipin'] = ["Google Map (Multilocation)", "Google Map (Multilocation)"];

$GLOBALS['TL_LANG']['tl_content']['dse_zoomfactor'] =
    ['Zoom factor', 'Please enter the zoom factor.'];

$GLOBALS['TL_LANG']['tl_content']['location1_legend']   = 'First Location';
$GLOBALS['TL_LANG']['tl_content']['dse_location1_latitude'] =
    ['Latitude', 'Please enter the latitude value.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_longitude'] =
    ['Longitude', 'Please enter the Longitude value.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_company'] =
    ['Title', 'Please enter the Title info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_street'] =
    ['Street/Number', 'Please enter the Street/Number info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_zip'] =
    ['ZIP/City', 'Please enter the ZIP/City info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_partnername'] =
    ['Partner Name', 'Please enter the Name of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_phone'] =
    ['Partner Phone', 'Please enter the Phone of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_mail'] =
    ['Partner Email', 'Please enter the Email of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location1_maplink'] =
    ['Roadmap link', 'Here you can add a roadmap link.'];

$GLOBALS['TL_LANG']['tl_content']['location2_legend']   = 'Second Location';
$GLOBALS['TL_LANG']['tl_content']['dse_location2_latitude'] =
    ['Latitude', 'Please enter the latitude value.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_longitude'] =
    ['Longitude', 'Please enter the Longitude value.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_company'] =
    ['Title', 'Please enter the Title info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_street'] =
    ['Street/Number', 'Please enter the Street/Number info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_zip'] =
    ['ZIP/City', 'Please enter the ZIP/City info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_partnername'] =
    ['Partner Name', 'Please enter the Name of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_phone'] =
    ['Partner Phone', 'Please enter the Phone of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_mail'] =
    ['Partner Email', 'Please enter the Email of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location2_maplink'] =
    ['Roadmap link', 'Here you can add a roadmap link.'];

$GLOBALS['TL_LANG']['tl_content']['location3_legend']   = 'Third Location';
$GLOBALS['TL_LANG']['tl_content']['dse_location3_latitude'] =
    ['Latitude', 'Please enter the latitude value.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_longitude'] =
    ['Longitude', 'Please enter the Longitude value.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_company'] =
    ['Title', 'Please enter the Title info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_street'] =
    ['Street/Number', 'Please enter the Street/Number info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_zip'] =
    ['ZIP/City', 'Please enter the ZIP/City info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_partnername'] =
    ['Partner Name', 'Please enter the Name of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_phone'] =
    ['Partner Phone', 'Please enter the Phone of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_mail'] =
    ['Partner Email', 'Please enter the Email of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location3_maplink'] =
    ['Roadmap link', 'Here you can add a roadmap link.'];

$GLOBALS['TL_LANG']['tl_content']['location4_legend']   = 'Fourth Location';
$GLOBALS['TL_LANG']['tl_content']['dse_location4_latitude'] =
    ['Latitude', 'Please enter the latitude value.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location4_longitude'] =
    ['Longitude', 'Please enter the Longitude value.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location4_company'] =
    ['Title', 'Please enter the Title info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location4_street'] =
    ['Street/Number', 'Please enter the Street/Number info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location4_zip'] =
    ['ZIP/City', 'Please enter the ZIP/City info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location4_partnername'] =
    ['Partner Name', 'Please enter the Name of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location4_phone'] =
    ['Partner Phone', 'Please enter the Phone of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location4_mail'] =
    ['Partner Email', 'Please enter the Email of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location4_maplink'] =
    ['Roadmap link', 'Here you can add a roadmap link.'];

$GLOBALS['TL_LANG']['tl_content']['location5_legend']   = 'Fifth Location';
$GLOBALS['TL_LANG']['tl_content']['dse_location5_latitude'] =
    ['Latitude', 'Please enter the latitude value.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_longitude'] =
    ['Longitude', 'Please enter the Longitude value.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_company'] =
    ['Title', 'Please enter the Title info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_street'] =
    ['Street/Number', 'Please enter the Street/Number info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_zip'] =
    ['ZIP/City', 'Please enter the ZIP/City info.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_partnername'] =
    ['Partner Name', 'Please enter the Name of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_phone'] =
    ['Partner Phone', 'Please enter the Phone of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_mail'] =
    ['Partner Email', 'Please enter the Email of the Partner.'];

$GLOBALS['TL_LANG']['tl_content']['dse_location5_maplink'] =
    ['Roadmap link', 'Here you can add a roadmap link.'];