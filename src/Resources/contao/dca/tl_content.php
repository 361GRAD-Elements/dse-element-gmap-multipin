<?php

/**
 * 361GRAD Element Gmapmultipin
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_gmapmultipin'] =
    '{type_legend},type,headline;' .
    '{gmap_legend},dse_zoomfactor;' .
    '{location1_legend},dse_location1_latitude,dse_location1_longitude,dse_location1_company,dse_location1_street,dse_location1_zip,dse_location1_partnername,dse_location1_phone,dse_location1_mail,dse_location1_maplink;' .
    '{location2_legend},dse_location2_latitude,dse_location2_longitude,dse_location2_company,dse_location2_street,dse_location2_zip,dse_location2_partnername,dse_location2_phone,dse_location2_mail,dse_location2_maplink;' .
    '{location3_legend},dse_location3_latitude,dse_location3_longitude,dse_location3_company,dse_location3_street,dse_location3_zip,dse_location3_partnername,dse_location3_phone,dse_location3_mail,dse_location3_maplink;' .
    '{location4_legend},dse_location4_latitude,dse_location4_longitude,dse_location4_company,dse_location4_street,dse_location4_zip,dse_location4_partnername,dse_location4_phone,dse_location4_mail,dse_location4_maplink;' .
    '{location5_legend},dse_location5_latitude,dse_location5_longitude,dse_location5_company,dse_location5_street,dse_location5_zip,dse_location5_partnername,dse_location5_phone,dse_location5_mail,dse_location5_maplink;' .
    '{invisible_legend:hide},invisible,start,stop,dse_data';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_zoomfactor'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_zoomfactor'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location1_latitude'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location1_latitude'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => true,
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location1_longitude'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location1_longitude'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => true,
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location1_company'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location1_company'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location1_street'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location1_street'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location1_zip'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location1_zip'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location1_partnername'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location1_partnername'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location1_phone'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location1_phone'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location1_mail'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location1_mail'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location1_maplink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location1_maplink'],
    'search'    => true,
    'inputType' => 'textarea',
    'eval'      => [
        'allowHtml'=>true,
        'rte'=>'ace|html',
        'class'=>'monospace',
        'tl_class'  => 'clr w100'
    ],
    'sql'       => "mediumtext NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location2_latitude'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location2_latitude'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location2_longitude'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location2_longitude'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location2_company'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location2_company'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location2_street'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location2_street'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location2_zip'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location2_zip'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location2_partnername'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location2_partnername'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location2_phone'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location2_phone'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location2_mail'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location2_mail'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location2_maplink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location2_maplink'],
    'search'    => true,
    'inputType' => 'textarea',
    'eval'      => [
        'allowHtml'=>true,
        'rte'=>'ace|html',
        'class'=>'monospace',
        'tl_class'  => 'clr w100'
    ],
    'sql'       => "mediumtext NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location3_latitude'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location3_latitude'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location3_longitude'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location3_longitude'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location3_company'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location3_company'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location3_street'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location3_street'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location3_zip'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location3_zip'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location3_partnername'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location3_partnername'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location3_phone'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location3_phone'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location3_mail'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location3_mail'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location3_maplink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location3_maplink'],
    'search'    => true,
    'inputType' => 'textarea',
    'eval'      => [
        'allowHtml'=>true,
        'rte'=>'ace|html',
        'class'=>'monospace',
        'tl_class'  => 'clr w100'
    ],
    'sql'       => "mediumtext NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location4_latitude'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location4_latitude'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location4_longitude'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location4_longitude'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location4_company'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location4_company'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location4_street'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location4_street'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location4_zip'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location4_zip'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location4_partnername'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location4_partnername'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location4_phone'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location4_phone'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location4_mail'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location4_mail'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location4_maplink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location4_maplink'],
    'search'    => true,
    'inputType' => 'textarea',
    'eval'      => [
        'allowHtml'=>true,
        'rte'=>'ace|html',
        'class'=>'monospace',
        'tl_class'  => 'clr w100'
    ],
    'sql'       => "mediumtext NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location5_latitude'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location5_latitude'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location5_longitude'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location5_longitude'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location5_company'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location5_company'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location5_street'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location5_street'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location5_zip'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location5_zip'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location5_partnername'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location5_partnername'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location5_phone'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location5_phone'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location5_mail'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location5_mail'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_location5_maplink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_location5_maplink'],
    'search'    => true,
    'inputType' => 'textarea',
    'eval'      => [
        'allowHtml'=>true,
        'rte'=>'ace|html',
        'class'=>'monospace',
        'tl_class'  => 'clr w100'
    ],
    'sql'       => "mediumtext NULL"
];
