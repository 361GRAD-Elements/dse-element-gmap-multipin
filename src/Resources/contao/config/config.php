<?php

/**
 * 361GRAD Element Gmapmultipin
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_gmapmultipin'] =
    'Dse\\ElementsBundle\\ElementGmapmultipin\\Element\\ContentDseGmapmultipin';

// Cascading Style Sheets
$GLOBALS['TL_CSS']['dse_gmapmultipin'] = 'bundles/dseelementgmapmultipin/css/fe_ce_dse_gmapmultipin.css';
